package com.uzengroup.jkd.portal.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.uzengroup.jkd.cache.portal.impl.OrderByIdCacheManager;
import com.uzengroup.jkd.core.Constants;
import com.uzengroup.jkd.core.Constants.PaymentType;
import com.uzengroup.jkd.core.model.Order;
import com.uzengroup.jkd.core.model.OrderPaymentTypeRecord;
import com.uzengroup.jkd.core.model.WeChatAppConfig;
import com.uzengroup.jkd.core.service.OrderManager;
import com.uzengroup.jkd.core.service.OrderPaymentTypeRecordManager;
import com.uzengroup.jkd.core.util.BaseThreadPool;
import com.uzengroup.jkd.core.util.StringUtil;
import com.uzengroup.jkd.core.vo.MsgModel;
import com.uzengroup.jkd.portal.controller.OrderPayNotifyController;
import com.uzengroup.jkd.portal.util.AliPayUtil;
import com.uzengroup.jkd.portal.util.WeiXinPayUtil;

/**
 * 待支付订单异常检查
 * 查询支付宝、微信支付结果
 * 每5分钟执行定时任务
 * @author chao.huang
 */
@Component
public class AutoOrderWaitPaymentCheckTask {
	protected final transient Log log = LogFactory.getLog(getClass());
	@Autowired
	private OrderManager orderManager;
	
	@Autowired
	private OrderPaymentTypeRecordManager orderPaymentTypeRecordManager;
	
	@Autowired
	OrderByIdCacheManager orderByIdCacheManager;
	
	@Scheduled(cron="0 0/5 * * * *")
	public void execute(){
		log.info("AutoOrderWaitPaymentCheckTask ........");
		int size = 0;
		try{
			List<OrderPaymentTypeRecord> recordIdList = this.orderPaymentTypeRecordManager.updateOrderPaymentTypeByLoadFunction();
			if(recordIdList != null && recordIdList.size() > 0){
				size = recordIdList.size();
				Map<Long, List<OrderPaymentTypeRecord>> orderIdPaymentRecordMap = new HashMap<Long, List<OrderPaymentTypeRecord>>();
				for(OrderPaymentTypeRecord record : recordIdList){
					Long orderId = record.getOrderId();
					List<OrderPaymentTypeRecord> list = orderIdPaymentRecordMap.get(orderId);
					if(list == null){
						orderIdPaymentRecordMap.put(orderId, list = new ArrayList<OrderPaymentTypeRecord>());
					}
					list.add(record);
				}
				final Map<Long, List<OrderPaymentTypeRecord>> orderIdPaymentRecordMapBak = orderIdPaymentRecordMap;
				for(final Long orderId : orderIdPaymentRecordMapBak.keySet()){
					BaseThreadPool.getThreadPoolExecutor().execute(new Runnable(){
						@Override
						public void run() {
							List<OrderPaymentTypeRecord> list = orderIdPaymentRecordMapBak.get(orderId);
							if(CollectionUtils.isEmpty(list)){
								return;
							}
							Order order = orderManager.get(orderId);
							if(order == null || order.getOrderId() == null || StringUtil.nullToBoolean(order.getIsDelete())){
								return;
							}
							Boolean isSucc = StringUtil.nullToBoolean(order.getIsPaymentSucc());
							boolean isPaymentSucc = false;
							if(isSucc){
								//订单已经支付成功，删除订单对应的支付记录
								isPaymentSucc = true;
							}else{
								for(OrderPaymentTypeRecord record : list){
									try{
										Integer paymentType = record.getPaymentType();
										Long weChatConfigId = record.getWeChatConfigId();
										if(StringUtil.compareObject(paymentType, PaymentType.PAYMENT_TYPE_WECHAT)){
											//微信支付
											WeChatAppConfig weChatAppConfig = Constants.WECHAT_CONFIG_ID_MAP.get(weChatConfigId);
											if(weChatAppConfig != null && weChatAppConfig.getConfigId() != null){
												//根据支付流水号获取支付信息
												int orderTotal = WeiXinPayUtil.orderAmountToBranch(order.getOrderAmount());
												MsgModel<String> msModel = WeiXinPayUtil.getQueryPayInfo(weChatAppConfig, order.getTradeNo(), order.getOrderNo());
												if(StringUtil.nullToBoolean(msModel.getIsSucc()) && StringUtil.compareObject(msModel.getData(), orderTotal)){
													OrderPayNotifyController.updateOrderPaymentSuccStatus(order.getOrderId(), order.getOrderNo(), order.getTradeNo(), PaymentType.PAYMENT_TYPE_WECHAT, weChatAppConfig.getConfigId());
													isPaymentSucc = true;
													break;
												}
											}
										}else if(StringUtil.compareObject(paymentType, PaymentType.PAYMENT_TYPE_ALIPAY)){
											//支付宝支付
											//根据支付流水号获取支付信息
											MsgModel<String> msModel = AliPayUtil.getQueryAliPayInfo(order.getOrderNo(), order.getTradeNo());
											if(StringUtil.nullToBoolean(msModel.getIsSucc()) && StringUtil.compareObject(msModel.getData(), StringUtil.formatDouble2Str(order.getOrderAmount()))){
												OrderPayNotifyController.updateOrderPaymentSuccStatus(order.getOrderId(), order.getOrderNo(), order.getTradeNo(), PaymentType.PAYMENT_TYPE_ALIPAY, null);
												isPaymentSucc = true;
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}
							
							if(isPaymentSucc){
								List<Long> idList = new ArrayList<Long>();
								for(OrderPaymentTypeRecord record : list){
									Long id = record.getId();
									idList.add(id);
								}
								orderPaymentTypeRecordManager.deleteByIdList(idList);
							}
						}
					});
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		log.info("AutoOrderWaitPaymentCheckTask  === " + String.format("[size=%s]", size));
	}
}
